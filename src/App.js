import React, { Component } from 'react';
import './App.css';

import Homepages from './Components/Homepages/Homepages';
class App extends Component {
  render() {
    return (
      <div className="App">
       <Homepages/>
      </div>
    );
  }
}

export default App;
