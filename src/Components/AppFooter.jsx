import React from 'react';
import { Card, Button, CardHeader, CardFooter, CardBlock,
CardTitle, CardText, Container, Row, Col, CardImg, CardLink} from 'reactstrap';

const Example = (props) => {
  return (
      <div>
          <Container fluid className='footer-mbuh text-center'>
              <Row>
                <Col md='2'>
                <CardImg className='content-footer' top width="100%" src='https://www.dropbox.com/s/zf0e60dqacejj8o/LogoLightGrey%401x.png?raw=1' />
                </Col>
                <Col md='2'>
                <ul className='jancuuuk'>
                    <li className="list-unstyled">
                        <a className='link-footer' href="#!">Investasi</a>
                    </li>
                    <li className="list-unstyled">
                        <a className='link-footer' href="#!">Cara Kerja</a>
                    </li>
                    <li className="list-unstyled">
                        <a className='link-footer' href="#!">Tentang Kami</a>
                    </li>
                </ul>
                </Col>
                <Col md='2'>
                    <ul className='jancuuuk'>
                        <li className='list-unstyled'>
                            <a className='link-footer'  href='#!'>Bantuan</a>
                        </li>
                        <li className='list-unstyled'>
                            <a className='link-footer' href='#!'>Kontak Kami</a>
                        </li>
                    </ul>
                </Col>
                <Col md='6'>
                <ul className='jancuuuk'>
                    <li className='list-unstyled'>
                        <p className='par-footer'>Download Aplikasi Smartphone</p>
                    </li>
                <CardLink href='/'>
                <img src="http://icons.iconarchive.com/icons/tristan-edwards/sevenesque/1024/App-Store-icon.png" height="50"/>
                </CardLink>
                <CardLink href='/'>
                <img src="https://image.flaticon.com/icons/png/512/355/355999.png" height="45"/>
                </CardLink>
                </ul>
                </Col>
              </Row>
          </Container>

          
        
        <CardFooter className="footer-copyright text-center py-3">
            <Container fluid>
                &copy; {new Date().getFullYear()} Copyright - {" "}
                <a> PT Monggovest Indonesia </a>
            </Container>
        </CardFooter>
    
          </div>
    
  );
};


export default Example;