import React from 'react';
import { Card, Button, CardTitle, CardText, Row, Col, CardImg } from 'reactstrap';

const Example = (props) => {
  return (
    <Row>
      <Col sm="4">
      <CardImg top width="100%" src='https://cdn-images-1.medium.com/max/1200/1*y03gw83Wj4BYk_sD75gNEA.png' />
        <Card body>
          <CardTitle>Special Title Treatment</CardTitle>
          <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
          <Button>Go somewhere</Button>
        </Card>
      </Col>
      <Col sm="4">
      <CardImg top width="100%" src='https://cdn-images-1.medium.com/max/1200/1*y03gw83Wj4BYk_sD75gNEA.png' />
        <Card body>
          <CardTitle>Special Title Treatment</CardTitle>
          <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
          <Button>Go somewhere</Button>
        </Card>
      </Col>
      <Col sm="4">
      <CardImg top width="100%" src='https://cdn-images-1.medium.com/max/1200/1*y03gw83Wj4BYk_sD75gNEA.png' />
        <Card body>
          <CardTitle>Special Title Treatment</CardTitle>
          <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
          <Button>Go somewhere</Button>
        </Card>
      </Col>
    </Row>
  );
};

export default Example;