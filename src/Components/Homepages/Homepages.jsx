import React from 'react';
import AppHeader from '../AppHeader';
import AppCarousell from './AppCarousell';
import Kartu from './Kartu';
import KartuLagi from './KartuLagi';
import AppFooter from '../AppFooter';
import{
    Button
}from 'reactstrap';

class Homepages extends React.Component{
    render(){
        return(
            <div>
                <AppHeader/>
                <AppCarousell/>
                <h1 className='judul' >LANGKAH MUDAH BERINVESTASI</h1>
                <Kartu/>
                <Button className='butt' >LIHAT INVESTASI</Button>{' '}
                <h1 className='judul'>TERPOPULER</h1>
                <KartuLagi/>
                <Button className='butt' >LIHAT INVESTASI</Button>{' '}
                <AppFooter/>
            </div>
        )
    }
}

export default Homepages;