import React from 'react';
import { Card, CardTitle, CardText, Row, Col, CardImg, Container } from 'reactstrap';

const Kartu = (props) => {
  return (
      <Container>
      <Row>
      <Col sm="3">
      <CardImg top width="100%" src='https://www.dropbox.com/s/xg0fa86w9deja6m/Invest.svg?raw=1' className="rounded-circle" />
        <Card body className='card-step'>
          <CardTitle>Pilih Instrumen Investasi</CardTitle>
          
        </Card>
      </Col>
      <Col sm="3">
      <CardImg top width="100%" src='https://www.dropbox.com/s/2tim8bj431bceak/Pay.svg?raw=1' className="rounded-circle" />
        <Card body className='card-step'>
          <CardTitle>Lakukan </CardTitle>
          <CardTitle>Pembayaran</CardTitle>
          
        </Card>
      </Col>
      <Col sm="3" >
      <CardImg top width="100%" src='https://www.dropbox.com/s/zr9o1ggykpkanra/Cow.svg?raw=1' className="rounded-circle" />
        <Card body className='card-step'>
          <CardTitle>Modal Sampai ke Peternak</CardTitle>
          
        </Card>
      </Col>
      <Col sm="3">
      <CardImg top width="100%" src='https://www.dropbox.com/s/t3cti8tf74ni2fz/Result.svg?raw=1' className="rounded-circle" />
        <Card body className='card-step'>
          <CardTitle>Tunggu Hasilnya</CardTitle>
          
        </Card>
      </Col>
    </Row>
    </Container>
  );
};

export default Kartu;