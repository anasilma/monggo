import React, { Component } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption,
  Button
  
} from 'reactstrap';

const items = [
  {
    src: 'https://www.dropbox.com/s/slve46v6poxyazp/Image.jpg?raw=1',
    altText: 'Slide 1',
    caption: 'Kelola keuangan anda dengan berinvestasi, ora udud karo ngopi wae.',
    header: 'SEMUA ORANG BISA BETERNAK',
       
    
  },
  {
    src: 'https://www.dropbox.com/s/slve46v6poxyazp/Image.jpg?raw=1',
    altText: 'Slide 2',
    caption: 'Kelola keuangan anda dengan berinvestasi, ora udud karo ngopi wae.',
    header: 'SEMUA ORANG BISA BETERNAK'
  },
  {
    src: 'https://www.dropbox.com/s/slve46v6poxyazp/Image.jpg?raw=1',
    altText: 'Slide 3',
    caption: 'Kelola keuangan anda dengan berinvestasi, ora udud karo ngopi wae.',
    header: 'SEMUA ORANG BISA BETERNAK'
  }
];

class CarouselJancuk extends Component {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  render() {
    
    const { activeIndex } = this.state;

    const slides = items.map((item) => {
      return (
        
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.src}
          
        >
          
          <img src={item.src} alt={item.altText} />
          <CarouselCaption  captionText={item.caption} captionHeader={item.header} />
        </CarouselItem>
      );
    });

    return (
      
      <Carousel
        activeIndex={activeIndex}
        next={this.next}
        previous={this.previous}
        
      >
        <CarouselIndicators className='round-indik' items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
        {slides}
        <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
        <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
      </Carousel>
    );
  }
}


export default CarouselJancuk;