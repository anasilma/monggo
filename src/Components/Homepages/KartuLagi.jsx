import React from 'react';
import { Card, Button, CardTitle, CardText, Row, Col, CardImg, Container, CardBody } from 'reactstrap';

const KartuLagi = (props) => {
  return (
      <Container>
      <Row>
      <Col sm="4" >
      <Card>
      <CardImg className='img-popular' src='https://cdn-images-1.medium.com/max/1200/1*y03gw83Wj4BYk_sD75gNEA.png' />
        <CardBody className='card-popular' >
          <CardTitle>Special Title Treatment</CardTitle>
          <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
          <Button className='button-popular'>Details</Button>
        </CardBody>
      </Card>
      </Col>
      <Col sm="4">
      <Card>
      <CardImg className='img-popular' src='https://cdn-images-1.medium.com/max/1200/1*y03gw83Wj4BYk_sD75gNEA.png' />
        <CardBody>
          <CardTitle>Special Title Treatment</CardTitle>
          <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
          <Button className='button-popular'>Details</Button>
        </CardBody>
      </Card>
      </Col>
      <Col sm="4">
      <Card>
      <CardImg className='img-popular' src='https://cdn-images-1.medium.com/max/1200/1*y03gw83Wj4BYk_sD75gNEA.png'/>
        <CardBody>
          <CardTitle>Special Title Treatment</CardTitle>
          <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
          <Button className='button-popular'>Details</Button>
        </CardBody>
      </Card>
      </Col>
    
    </Row>
    </Container>
  );
};

export default KartuLagi;