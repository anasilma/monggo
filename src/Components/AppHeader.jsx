import React, { Component } from 'react';
import {

    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    
    Container
} from 'reactstrap';
import './Test.css';
import{Link, Router} from 'react-router-dom';

class AppHeader extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
        return (
            <div>
                    <Navbar className='bg-light' color='light' light expand='md' fixed='top'>
                    <NavbarBrand href='/' className='napbar'>
                    <img src="https://www.dropbox.com/s/zf0e60dqacejj8o/LogoLightGrey%401x.png?raw=1" height="35"/>
                    </NavbarBrand>
                        <NavbarToggler onClick={this.toggle} />
                        <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav  navbar>
                                <NavItem >
                                    <Link className='head-app' to='/'>Home</Link>
                                </NavItem>
                                <NavItem>
                                    <Link className='head-app' to='/about' >About</Link>
                                </NavItem>
                                <NavItem>
                                    <NavLink className='head-app' >Contact Us</NavLink>
                                </NavItem>
                            </Nav>
                        </Collapse>
                    </Navbar>
            
                </div>
        )
    }
}

export default AppHeader;