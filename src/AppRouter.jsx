import React from 'react';
import {
    BrowserRouter,
    Route
} from 'react-router-dom';
import App from './App'
import AppAbout from './Components/About/AppAbout';

class AppRouter extends React.Component{
    render(){
        return(
            <div>
                <BrowserRouter>
                    <div>
                        <Route exact path='/' component={App}  />
                        <Route path='/about' component={AppAbout}  />
                    </div>
                </BrowserRouter>
            </div>
        )
    }
}

export default AppRouter;